package main

import (
	"fmt"
	"math"
)

func main() {
	// Variable declarations
	var name string
	var n1, n2, n3, n4 int

	// Get user input for name
	name = getUserInput("Enter your name: ")

	// Get user input for numbers
	num1 = getUserInputInt("Enter the first number: ")
	num2 = getUserInputInt("Enter the second number: ")
	num3 = getUserInputInt("Enter the number for square root calculation: ")
	num4 = getUserInputInt("Enter the number to check if it's even: ")

	// Call various functions
	greet(name)

	sum, err := add(num1, num2)
	if err != nil {
		fmt.Println("Error adding numbers:", err)
		return
	}

	fmt.Println("Sum:", sum)

	squareRoot, err := calculatesquareroot(float64(num3))
	if err != nil {
		fmt.Println("Error calculating square root:", err)
		return
	}
	fmt.Println("Square root:", squareRoot)

	isEven, err := isnumbereven(num4)
	if err != nil {
		fmt.Println("Error checking if number is even:", err)
		return
	}
	fmt.Println("Is", num4, "even?", isEven)

	if isEven {
		fmt.Println(num4, "is even.")
	} else {
		fmt.Println(num4, "is odd.")
	}

	numbers := []int{5, 3, 8, 2, 1, 4}
	fmt.Println("Unsorted array:", numbers)

	// Sorting the array
	bubblesort(numbers)

	fmt.Println("Sorted array:", numbers)

}

// Get user input for a string
func GetUserInput(prompt string) string {
	var input string
	fmt.Print(prompt)
	fmt.Scan(&input)
	return input
}

// Get user input for an integer
func GetUserInputInt(prompt string) int {
	var input int
	fmt.Print(prompt)
	fmt.Scan(&input)
	return input
}

// Greet a person
func Greet(name string) {
	fmt.Println("Hello,", name+"!")
}

// Add two numbers
func Add(a, b int) (int, error) {
	return a + b, nil
}

// Calculate Square Root
func CalculateSquareRoot(num float64) (float64, error) {
	if num < 0 {
		return 0, fmt.Errorf("cannot calculate square root of a negative number")
	}
	return math.Sqrt(num), nil
}

// Checks if number is even
func Isnumbereven(num int) (bool, error) {
	if num%2 == 0 {
		return true, nil
	} else {
		return false, nil
	}
}

func bubblesort(arr []int) {
	n := len(arr)
	for i := 0; i < n-1; i++ {
		for j := 0; j < n-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
}
